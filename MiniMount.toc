## Interface: 70200
## Title: MiniMount
## Title-deDE: MiniMount
## Title-frFR: MiniMount
## Author: Kjasi
## Version: <%version%>
## Notes: Random mount selection, based on location and skill.
## Notes-deDE: Random Mount Auswahl, vor Ort und Geschicklichkeit.
## Notes-frFR: S�lection al�atoire de montage, en fonction de l'emplacement et de la comp�tence.
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## DefaultState: Enabled
## LoadOnDemand: 0
##
#  myAddOns Support Metadata
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Category: Information
##
## SavedVariables: MiniMount_Mounts, MiniMount_Options
Load.xml