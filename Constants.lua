--[[
	MiniMount Constants
	Version <%version%>
	
	Revision: $Id: Constants.lua 10 2016-09-30 08:42:31 Pacific Daylight Time Kjasi $
]]

-- Initialize our Addon
MiniMount = {}
local m = MiniMount

m.PlayerData = {}
local P = m.PlayerData

-- Addon Name. This is NOT localized.
m.MINIMOUNT_TITLE = "MiniMount"

-- Base-level variables
m.Version = "<%version%>"
m.Debug = 0	-- 0 = no messages.
m.Ready = false
m.Map = {}
m.Holiday = nil
m.IsLoaded = nil
m.Interface = {}
m.CurrentDB = {}
m.CurrentTypes = { 230 }
m.CurrentCommandMode = 01

-- Riding Skill Training Levels
m.TRAINRIDING_APPRENTICE	= 20
m.TRAINRIDING_JOURNEYMAN	= 40
m.TRAINRIDING_EXPERT		= 60
m.TRAINRIDING_ARTISAN		= 70
m.TRAINRIDING_MASTER		= 80
m.TRAINRIDING_FML			= 60
m.TRAINRIDING_CWF			= 68

-- Riding Skill Level
m.RIDINGSKILLLVL_APPRENTICE	= 75
m.RIDINGSKILLLVL_JOURNEYMAN	= 150
m.RIDINGSKILLLVL_EXPERT		= 225
m.RIDINGSKILLLVL_ARTISAN	= 300
m.RIDINGSKILLLVL_MASTER		= 375

-- Riding Skill Spell ID
m.RIDINGSPELLID_APPRENTICE	= 33388
m.RIDINGSPELLID_JOURNEYMAN	= 33391
m.RIDINGSPELLID_EXPERT		= 34090
m.RIDINGSPELLID_ARTISAN		= 34091
m.RIDINGSPELLID_MASTER		= 90265

m.RIDINGSPELLID_FML			= 90267		-- Flight Master's License (Azeroth Flying)
m.RIDINGSPELLID_CWF			= 54197		-- Cold Weather Flying (Northrend Flying)
m.RIDINGSPELLID_WFW			= 115913	-- Wind of the Four Winds (Pandaria Flying)
m.RIDINGSPELLID_CSR			= 130487	-- Cloud Serpent Riding (Serpent Riding)

-- Riding Speeds
m.Speed = {}
m.Speed.s0 = 0
m.Speed.s60 = 60
m.Speed.s100 = 100
m.Speed.s150 = 150
m.Speed.s280 = 280
m.Speed.s300 = 300
m.Speed.s310 = 310
m.Speed.s450 = 450

-- Continent IDs
m.CONTINENT_EASTERNKINGDOMS	= 1
m.CONTINENT_KALIMDOR		= 2
m.CONTINENT_OUTLAND			= 3
m.CONTINENT_NORTHREND		= 4
m.CONTINENT_DEEPHOLM		= 5
m.CONTINENT_PANDARIA		= 6
m.CONTINENT_DRAENOR			= 7

-- Zone IDs
m.ZoneIDs = {}

-- Outside Zone IDs
m.ZoneIDs.WINTERGRASP		= 501
m.ZoneIDs.KELPTHARFOREST	= 610
m.ZoneIDs.ABYSSALDEPTHS		= 614
m.ZoneIDs.SHIMMERINGEXPANSE	= 615

m.ZoneIDs.HELHEIM = 1022
m.ZoneIDs.THEFELHAMMER	= 1052
m.ZoneIDs.HEARTOFAZEROTH	= 1057

-- Instance/Raid IDs
m.ZoneIDs.THEOCULUS			= 528
m.ZoneIDs.AHNQIRAJ40		= 766

-- Non-Database Mount Types and Speed
-- Mounts listed in the database should NEVER use these!
m.TYPE_AUTO					= 01	-- Mount Type Selection, automatically chooses the best type of mount.
m.TYPE_RANDOM				= 02	-- Mount Type Selection, Random mount of any usable type.
m.TYPE_SWIM					= 03	-- Mount Type Selection, used to categorize the various swimming mounts.

-- Mount Types and Speed
-- Use these types in the Database.
m.TYPE_GROUNDSLOW			= 04	-- Currently used only for the Turtles. Speed = 100% Running speed.
m.TYPE_GROUND				= 05	-- Ground-only Mounts.
m.TYPE_SWIM60				= 06	-- Swim Speed = 60% Swim Speed.
m.TYPE_SWIM100				= 07	-- Swim Speed = 100% Swim Speed. Used by the Sea Turtle and Druid's Aquatic Form in water.
m.TYPE_SWIM300				= 08	-- Swim Speed = 300% Swim Speed. Used by the Subdued Seahorse while in Vashj'ir.
m.TYPE_SWIM450				= 09	-- Swim Speed = 450% Swim Speed. Used by the Abyssal Seahorse for Vashj'ir.
m.TYPE_FLYING				= 10	-- Mounts that can only be called in areas where you can fly.
m.TYPE_SHIFTING				= 11	-- Mounts that can be either Flying or Ground mounts. All of them can also be used underwater, but don't provide a speed bonus.

-- Mount Seat Count
m.SEATS_ONE					= 1
m.SEATS_TWO					= 2
m.SEATS_THREE				= 3

-- Professions
-- Used to identify mounts that require a profession to use.
m.PROF_TAILORING				= GetSpellInfo(3908)
m.PROF_ENGINEERING			= GetSpellInfo(4036)

-- Summoning Types
m.SUMMONTYPE_MOUNT		= "Mount"
m.SUMMONTYPE_SPELL			= "Spell"
m.SUMMONTYPE_ITEM			= "Item"

-- Defaults
m.Defaults = {
	["Options"] = {
		["Version"] = m.Version,
		["PreferFlightForm"] = true,			-- Druids Only. Use Flight form instead of flying mounts.
		["UseHolidayMounts"] = true,			-- Use only Holiday Mounts during Holidays (Only applies to mounts that fit the proper mount-type for the zone.)
		["DismountWhileFlying"] = false,		-- Allow Dismounting while Flying
		["UseFlyingWithGround"] = false,		-- Include flying mounts with Ground mounts.
	},
	["CharOptions"] = {
		["Use Mount List"] = true,						-- Use Mount Lists (if any) by default
		["Default Mount List"] = "Default",		-- Reference the name of the "default" mount list. If set to Default, then use all mounts.
		["MountLists"] = {}
	},
	["Mount List"] = {
		["Name"] = "New Mount List",
		["Desc"] = "A New List of Mounts to use",
		["Enabled"] = true,
		["zones"] = {},							-- If no Zone IDs are listed, then this mount list will not auto-activate. User must select this list as the default.
		["Mounts"] = {},
	},
	["Mount Data"] = {
		["Enabled"] = true,
		["Ratio"] = 1,								-- The number of times it's listed in the random-load array.
		["DBID"] = "Brown Horse",					-- The String name of the mount in the database. Default to Brown Horse, just in case...
	},
	["MountDB"] = {
		["NextAllianceMount"] = {},
		["NextHordeMount"] = {},
		["MountList"] = {},
	}
}

-- Player Data
P.Name = UnitName("player")
P.Realm = GetRealmName()
P.Faction = UnitFactionGroup("player")
_, P.Class, P.ClassID = UnitClass("player")
P.ProfInd = {}
P.ProfInfo = {}
P.ProfInfo.Prof1 = {}
P.ProfInfo.Prof2 = {}
P.ProfInfo.Archaeology = {}
P.ProfInfo.Fishing = {}
P.ProfInfo.Cooking = {}
P.ProfInfo.FirstAid = {}

m.ClassIDMap = {
	["WARRIOR"]		 	= 1,
	["PALADIN"]		 	= 2,
	["HUNTER"]			 	= 3,
	["ROGUE"]			 	= 4,
	["PRIEST"]			 	= 5,
	["DEATHKNIGHT"]	 	= 6,
	["SHAMAN"]			= 7,
	["MAGE"]				= 8,
	["WARLOCK"]			= 9,
	["MONK"]				= 10,
	["DRUID"]				= 11,
	["DEMONHUNTER"]	= 12,
}