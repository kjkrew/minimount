--[[
	MiniMount Database
	Version <%version%>
	
	Revision: $Id: Database.lua 24 2016-09-27 11:28:50 Pacific Daylight Time Kjasi $

	Various Databases and informations that are needed.

]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
local L = m.Localize

m.MountAdditiveDB = {
	-- Holidays
	[352] 	= { ["holiday"] = "Love is in the Air" },
	[430] 	= { ["holiday"] = "Noblegarden" },
	[201]	= { ["holiday"] = "Brewfest"  },
	[202]	= { ["holiday"] = "Brewfest"  },
	[226]	= { ["holiday"] = "Brewfest" },
	[219] 	= { ["holiday"] = "Hallows End" },
	
	-- Profession
	[285] 	= { ["prof"] = m.PROF_TAILORING, ["proflevel"] = 300 },		-- Flying Carpet
	[279] 	= { ["prof"] = m.PROF_TAILORING, ["proflevel"] = 300 },		-- Magnificent Flying Carpet
	[375] 	= { ["prof"] = m.PROF_TAILORING, ["proflevel"] = 300 },		-- Frosty Flying Carpet
	[205] 	= { ["prof"] = m.PROF_ENGINEERING, ["proflevel"] = 300 },
	[204] 	= { ["prof"] = m.PROF_ENGINEERING, ["proflevel"] = 375 },
	
	-- Class-Specific
	[41]			= { ["class"] = "PALADIN" },				-- Warhorse
	[84]			= { ["class"] = "PALADIN" },				-- Charger
	[150]		= { ["class"] = "PALADIN" },				-- Thalassian Warhorse
	[149]		= { ["class"] = "PALADIN" },				-- Thalassian Charger
	[350]		= { ["class"] = "PALADIN" },				-- Sunwalker Kodo
	[221]		= { ["class"] = "DEATHKNIGHT" },		-- Acherus Deathcharger
	[236]		= { ["class"] = "DEATHKNIGHT" },		-- Winged Steed of the Ebon Blade
	[17]			= { ["class"] = "WARLOCK" },				-- Felsteed
	[83]			= { ["class"] = "WARLOCK" },				-- Dreadsteed
	[780]		= { ["class"] = "DEMONHUNTER" },	-- Felsaber
	
	-- Multi-seat
	[240] 	= { ["seats"] = m.SEATS_TWO },			-- Horde Motorcycle
	[275] 	= { ["seats"] = m.SEATS_TWO },			-- Alliance Motorcycle
	[280] 	= { ["seats"] = m.SEATS_THREE },			-- Traveler's Tundra Mammoth (Alliance)
	[284]	= { ["seats"] = m.SEATS_THREE },			-- Traveler's Tundra Mammoth (Horde)
	[286]	= { ["seats"] = m.SEATS_THREE },			-- Grand Black War Mammoth (Alliance)
	[287]	= { ["seats"] = m.SEATS_THREE },			-- Grand Black War Mammoth (Horde)
	[288] 	= { ["seats"] = m.SEATS_THREE },			-- Grand Ice Mammoth (Horde)
	[289] 	= { ["seats"] = m.SEATS_THREE },			-- Grand Ice Mammoth (Alliance)
	[382] 	= { ["seats"] = m.SEATS_TWO},			-- X-53 Touring Rocket
	[407]	= { ["seats"] = m.SEATS_TWO},			-- Sandstone Drake (Vial of the Sands)
	[455]	= { ["seats"] = m.SEATS_TWO},			-- Obsidian Nightwing
	[460]	= { ["seats"] = m.SEATS_THREE },			-- Grand Expedition Yak
}

-- Spells Database
-- Old Format
m.MountSpellsDB = {
	["Aquatic Form (Druid)"]				= { ["id"] = 1066,			["types"] = { m.TYPE_SWIM100, TYPE_SWIM300 }},	-- Druid's Aquatic form nearly gives us the same speed as the 450% Seahorse.
	["Flight Form (Druid)"]					= { ["id"] = 33943,		["types"] = { m.TYPE_FLYING }},
	["Swift Flight Form (Druid)"]			= { ["id"] = 40120,		["types"] = { m.TYPE_FLYING }},
	["Running Wild (Worgen)"]				= { ["id"] = 87840,		["types"] = { m.TYPE_GROUND }},
	["Zen Flight (Monk)"]					= { ["id"] = 125883,		["types"] = { m.TYPE_FLYING }},
}

-- Item Database
-- Old Format
m.MountItemsDB = {
	["Flying Broom"]							= { ["id"] = 33176,			["types"] = { m.TYPE_FLYING },			["holiday"] = "Hallows End" },
	["Magic Broom"]							= { ["id"] = 37011,			["types"] = { m.TYPE_SHIFTING },		["holiday"] = "Hallows End" },
	["Oculus Emerald Essence"]			= { ["id"] = 37815,			["types"] = { m.TYPE_FLYING },			["zones"] = { m.ZoneIDs.THEOCULUS } },
	["Oculus Amber Essence"]				= { ["id"] = 37859,			["types"] = { m.TYPE_FLYING },			["zones"] = { m.ZoneIDs.THEOCULUS } },
	["Oculus Ruby Essence"]				= { ["id"] = 37860,			["types"] = { m.TYPE_FLYING },			["zones"] = { m.ZoneIDs.THEOCULUS } },
	["Dragonwrath, Tarecgosa's Rest"]	= { ["id"] = 71086,			["types"] = { m.TYPE_FLYING }, },
}