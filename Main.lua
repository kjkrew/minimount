--[[
	MiniMount Main Functions
	Version <%version%>

	Revision: $Id: Main.lua 30 2016-09-27 13:25:39 Pacific Daylight Time Kjasi $
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end

local L = m.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Localization.|r")
	return
end

local P = m.PlayerData

-- Libraries
local LibKJ = LibStub("LibKjasi-1.0")

-- Setup our addon with KjasiLib
LibKJ:setupAddon({
	["Name"] = m.MINIMOUNT_TITLE,
	["Version"] = m.Version,
	["MsgStrings"] = {
		["Default"] = GREEN_FONT_COLOR_CODE..L["Message Normal"],
		["error"] = RED_FONT_COLOR_CODE..L["Message Error"],
		["debug"] = LIGHTYELLOW_FONT_COLOR_CODE..L["Message Debug"],
	},
	["ChannelLevel"] = m.Debug,
})

function MiniMount:Msg(message, channel, debug)
	if (message == nil) then return end
	LibKJ:Msg(m.MINIMOUNT_TITLE, message, channel, debug)
end

-- Loading Function
function MiniMount_Load(self)
	self:RegisterEvent("VARIABLES_LOADED")
	self:RegisterEvent("COMPANION_LEARNED")
	self:RegisterEvent("ZONE_CHANGED_NEW_AREA")
	self:RegisterEvent("KNOWLEDGE_BASE_SYSTEM_MOTD_UPDATE")
	self:RegisterEvent("TIME_PLAYED_MSG")
	self:RegisterEvent("PLAYER_ENTERING_WORLD")
	self:RegisterEvent("PLAYER_REGEN_DISABLED")
	self:RegisterEvent("PLAYER_REGEN_ENABLED")
end
	
function MiniMount_OnEvent(self,event, ...)
	local arg1, arg2, arg3, _ = ...

	if (m.IsLoaded ~= 1) and (event ~= "VARIABLES_LOADED") then
		return
	end

	if (event == "VARIABLES_LOADED") then
		MiniMount:Msg("OnEvent:Loaded","debug",1)
		m:Loaded()
		m.IsLoaded = 1
		self:UnregisterEvent("VARIABLES_LOADED")
	elseif event == "KNOWLEDGE_BASE_SYSTEM_MOTD_UPDATE" or event == "TIME_PLAYED_MSG" then
		if (UnitLevel("player") < m.TRAINRIDING_APPRENTICE) or (P.RidingSkill < m.RIDINGSKILLLVL_APPRENTICE) then return end
		m:SetMapCurrent(true)
		MiniMount_Macro()
		m.IsLoaded = 1
		self:UnregisterEvent("KNOWLEDGE_BASE_SYSTEM_MOTD_UPDATE")
		self:UnregisterEvent("TIME_PLAYED_MSG")
	elseif (event == "COMPANION_LEARNED") then
		m:FillMountDatabase(true)
		m:GetNextMount()
	elseif (event == "ZONE_CHANGED_NEW_AREA") then
		if (UnitLevel("player") < m.TRAINRIDING_APPRENTICE) or (P.RidingSkill < m.RIDINGSKILLLVL_APPRENTICE) then return end
		m:SetMapCurrent(true)
		m:GetNextMount()
		MiniMount_Macro()
	elseif (event == "PLAYER_ENTERING_WORLD") then
		CalendarFrame:Show()
		CalendarFrame:Hide()
		self:UnregisterEvent("PLAYER_ENTERING_WORLD")
	elseif (event == "PLAYER_REGEN_DISABLED") then
		m.CombatFlagged = 1
	elseif (event == "PLAYER_REGEN_ENABLED") then
		m.CombatFlagged = nil
	end
end

function m:BuildDatabase(database, defaults)
	if (not database) then
		database = defaults
	end
end

function m:UpdateDatabase(database, defaults)
	if (not MiniMount_Options) or (not MiniMount_Options["Global"]) or (not MiniMount_Options["Global"]["Version"]) or (MiniMount_Options["Global"]["Version"] < "2.0") then
		MiniMount_Options = {}
		MiniMount_Options["Global"] = m.Defaults["Options"]
		return
	end
	
	local temp = LibKJ:UpdateDatabase(database, defaults)
	temp["Version"] = m.Version
	MiniMount_Options = {} -- Just in case, erase all previous settings.
	MiniMount_Options = temp
end

function MiniMount_bcload()
	--print("Loading Collections")
	local BCol = UIParentLoadAddOn("Blizzard_Collections")
	MountJournal_UpdateMountList();
	m:FillMountDatabase()
	return BCol
end

function MiniMount_CheckHolidays()
	m:SetHoliday()
	m:GetNextMount()
	MiniMount_Macro()
end

function m:Loaded()
	MiniMount:Msg("Initially Loaded... Processing Variables...","debug",1)

	m:GetPlayerData()
	if (not MiniMount_Options or not MiniMount_Options["Global"]) then
		MiniMount_Options = {}
		MiniMount_Options["Global"] = m.Defaults["Options"]
	end
	if (not MiniMount_Options["Global"]["Version"]) or (MiniMount_Options["Global"]["Version"] < m.Version) then
		m:UpdateDatabase(MiniMount_Options, m.Defaults["Options"])
	end
	if (not MiniMount_Mounts or MiniMount_Mounts["Next"..P.Faction.."Mount"] == 0) then
		m:BuildMountDatabase()
		m:FillMountDatabase()
		m:SelectNextMount()
	end

	local BCal = UIParentLoadAddOn("Blizzard_Calendar")
	LibKJ:Timer(m.MINIMOUNT_TITLE, "LoadCollectionsTimer", 2, "MiniMount_bcload")
	LibKJ:Timer(m.MINIMOUNT_TITLE, "CheckCalendar", 2, "MiniMount_CheckHolidays")
	
	m:InterfaceLoaded()
	
	MiniMount:Msg(format(L["MiniMount is Loaded"],tostring(m.Version)))
end

local function remdata()
	MiniMount_Mounts["data"] = {}
	for i=1, 5000 do
		local creatureName, spellID, icon, active, isUsable, sourceType, isFavorite, isFactionSpecific, faction, hideOnChar, isCollected, mountID = C_MountJournal.GetMountInfoByID(i)
		local creatureDisplayID, descriptionText, sourceText, isSelfMount, mountType = C_MountJournal.GetMountInfoExtraByID(i)
		if creatureName ~= nil then
			facstr = "Common"
			if (faction == 0) then facstr = "Horde" end
			if (faction == 1) then facstr = "Alliance" end
			local v = i.."_"..creatureName
			MiniMount_Mounts["data"][v] = {}
			MiniMount_Mounts["data"][v].Name = creatureName
			MiniMount_Mounts["data"][v].mountID = mountID
			MiniMount_Mounts["data"][v].spellID = spellID
			MiniMount_Mounts["data"][v].faction = facstr
			MiniMount_Mounts["data"][v].mountType = mountType
		end
	end
end


-- Slash Commands
SLASH_MiniMount1 = "/minimount"
SLASH_MiniMount2 = "/mount"
SLASH_MiniMount3 = "/mm"
SlashCmdList["MiniMount"] = function (incmd)
	-- Parse command-line Macro options!
	local cmd = SecureCmdOptionParse(incmd)
	if not cmd then return end

	-- Force lowercase to make it more user-proof.
	if cmd == nil then cmd = "" end
	cmd = strlower(cmd)

	if (cmd == L["commandline rescan"]) then
		m:FillMountDatabase(true)
		MiniMount_CheckHolidays()
	elseif (cmd == L["commandline options"]) or (cmd == "options") then
		InterfaceOptionsFrame_OpenToCategory(MiniMount_InterfaceOptionsFrame)
	elseif (cmd == L["commandline new"]) or (cmd == "new") or (cmd == "newmount") or (cmd == "new mount") then
		m:GetNextMount()
	elseif (cmd == L["commandline help"]) or (cmd == "help") then
		MiniMount:Msg(format(L["Help Base"],L["commandline ground"],L["commandline fly"],L["commandline random"]))
	elseif (cmd == L["commandline fly"]) or (cmd == "fly") then
		m:Mount(m.TYPE_FLYING,L["commandline fly"])
	elseif (cmd == L["commandline swim regular"]) or (cmd == "swim reg") or (cmd == "swim regular") then
		m:Mount(m.TYPE_SWIM60,L["commandline swim regular"])
	elseif (cmd == L["commandline swim"]) or (cmd == "swim") then
		m:Mount(m.TYPE_SWIM,L["commandline swim"])
	elseif (cmd == L["commandline ground"]) or (cmd == "ground") then
		m:Mount(m.TYPE_GROUND,L["commandline ground"])
	elseif (cmd == L["commandline random"]) or (cmd == "random") then
		m:Mount(m.TYPE_RANDOM,L["commandline random"])
	elseif (cmd == nil) or (cmd == "") then
		m:Mount(nil,1)

	-- These are used during development and shouldn't be used by the public
	elseif (cmd == "test") then
		m:SetMapCurrent(true)
		m:getCanFly()
		m:GetNextMount()
		MiniMount_Macro()
	elseif (cmd == "test2") then
		local mountIDs = C_MountJournal.GetMountIDs()
		
		MiniMount_Mounts.TestData = {}
		MiniMount_Mounts.TestData.RawIDs = mountIDs
		MiniMount_Mounts.TestData.Data = {}
		for k,mid in pairs(mountIDs) do
			local creatureName, spellID, icon, active, isUsable, sourceType, isFavorite, isFactionSpecific, faction, hideOnChar, isCollected, mountID = C_MountJournal.GetMountInfoByID(mid)
			MiniMount_Mounts.TestData.Data["ID"..mountID] = {
				["Name"] = tostring(creatureName),
				["Collected"] = tostring(isCollected),
				["Faction"] = tostring(faction)
			}
		end
		MiniMount:Msg("data read")
	elseif (cmd == "test3") then
		for id,data in pairs(m.CurrentDB) do
			MiniMount:Msg(format("Name: %s, ID: %s, Type: %s", tostring(data.name), tostring(data.mountID), tostring(data.mountType)))
		end
		
	else
		MiniMount:Msg(L["Error Wrong Option"],"error")
	end
end

function m:Mount(mode,iscmd)
	-- Fail if the player is not high enough to have a mount.
	if (UnitLevel("player") < m.TRAINRIDING_APPRENTICE and not MiniMount_Mounts.MountList.Common["mt284"] and not MiniMount_Mounts.MountList[P.Faction]["mt284"]) then
		MiniMount:Msg(L["Error Level Not High Enough"],"error")
		return
	end

	-- Fail if there are no mounts.
	if (C_MountJournal.GetNumMounts() < 1) then
		MiniMount:Msg(L["Error No Mounts Any"],"error")
		return
	end

	m.CurrentCommandMode = nil
	if ((mode ~= nil) and (mode ~= "")) or (m:NextMount() == nil) or (m:NextMount() == "error") then
		m.CurrentCommandMode = mode
		m:GetNextMount(mode)
	end

	if (m:NextMount() == "error") then
		return
	end

	m:CallMount(m:NextMount())
end

-- We can ONLY call SUMMONTYPE_MOUNT mounts from the code. Items and Spells must be ran from the macro.
function m:CallMount(mount)
	C_MountJournal.SummonByID(mount.mountID)
	m.CurrentMount = mount
	m:GetNextMount()
end

function m:GetNextMount(inmode)
	local mount = m:SelectNextMount(inmode)
	m:Timer("Macro",1,"MiniMount_Macro")
end

function MiniMount_Macro()
	if C_MountJournal.GetNumMounts() == 0 then return end
	if InCombatLockdown() then return end
	if (m:NextMount() == {}) then m:GetNextMount(m.CurrentCommandMode) end
	local MountName = GetSpellInfo(m:NextMount().spellID)
	
	local Prepare = m:MountPrepare()

	local text = "#showtooltip "..tostring(MountName).."\n/dismount [btn:2,nomod"
	if (MiniMount_Options.Global.DismountWhileFlying == false) then
		text = text..",noflying"
	end
	text = text.."][mod:ctrl]\n/stopmacro [btn:2,nomod][mod:ctrl]"..Prepare.."\n/mm [btn:3] new\n"
	text = text.."/mm [btn:1,nomod]\n"
	text = text.."/mm [btn:1,mod:alt] ground\n/mm [btn:2,mod:alt] fly"
	
	local index = GetMacroIndexByName(m.MINIMOUNT_TITLE)
	if index == 0 then
		CreateMacro(m.MINIMOUNT_TITLE,"inv_misc_questionmark",text,nil)
		MiniMount:Msg(L["Macro Created"])
	else
		EditMacro(index,m.MINIMOUNT_TITLE,"inv_misc_questionmark",text)
	end
end
