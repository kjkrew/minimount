--[[
	MiniMount Helper Functions
	Version <%version%>
	
	Revision: $Id: Helpers.lua 24 2016-09-30 08:42:49 Pacific Daylight Time Kjasi $
]]

local m = _G.MiniMount
local LibKJ = LibStub("LibKjasi-1.0")

if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end


local L = m.Localize
local z = m.ZoneIDs
local P = m.PlayerData

local function fillProfInfo(index, database)
	if index == nil or index < 0 then return end
	local n,_,skillLevel,maxSkillLevel = GetProfessionInfo(index)
	database.Index = index
	database.Name = n
	database.Level = skillLevel
	database.MaxLevel = maxSkillLevel
end

-- Get the current Riding Skill Level
function m:GetRidingSkill()
	-- Gather Base Names. Used for Detecting spells...
	local Apprentice_Name = GetSpellInfo(m.RIDINGSPELLID_APPRENTICE)		-- Regular
	local Journeyman_Name = GetSpellInfo(m.RIDINGSPELLID_JOURNEYMAN)		-- Epic
	local Expert_Name = GetSpellInfo(m.RIDINGSPELLID_EXPERT)				-- Regular Flying
	local Artisan_Name = GetSpellInfo(m.RIDINGSPELLID_ARTISAN)				-- Epic Flying
	local Master_Name = GetSpellInfo(m.RIDINGSPELLID_MASTER)				-- Fast Flying

	-- Check to see if the spells are in our spellbook.
	local Test_Apprentice = GetSpellInfo(Apprentice_Name)
	local Test_Journeyman = GetSpellInfo(Journeyman_Name)
	local Test_Expert = GetSpellInfo(Expert_Name)
	local Test_Artisan = GetSpellInfo(Artisan_Name)
	local Test_Master = GetSpellInfo(Master_Name)		-- Don't actually need to test for Master, since it uses the same mounts as Artisan.

	local skill = 1
	if (Test_Master ~= nil) then
		skill = m.RIDINGSKILLLVL_MASTER	
	elseif (Test_Artisan ~= nil) then
		skill = m.RIDINGSKILLLVL_ARTISAN
	elseif (Test_Expert ~= nil) then
		skill = m.RIDINGSKILLLVL_EXPERT
	elseif (Test_Journeyman ~= nil) then
		skill = m.RIDINGSKILLLVL_JOURNEYMAN
	elseif (Test_Apprentice ~= nil) then
		skill = m.RIDINGSKILLLVL_APPRENTICE
	end
	return skill
end

-- Get non-static player data. IE: Tradeskill info
function m:GetPlayerData()
	P.Level = UnitLevel("player")

	local prof1, prof2, archaeology, fishing, cooking, firstAid = GetProfessions()
	
	P.ProfInd.Prof1 = prof1
	P.ProfInd.Prof2 = prof2
	P.ProfInd.Archaeology = archaeology
	P.ProfInd.Fishing = fishing
	P.ProfInd.Cooking = cooking
	P.ProfInd.FirstAid = firstAid
	
	fillProfInfo(prof1, P.ProfInfo.Prof1)
	fillProfInfo(prof2, P.ProfInfo.Prof2)
	fillProfInfo(archaeology, P.ProfInfo.Archaeology)
	fillProfInfo(fishing, P.ProfInfo.Fishing)
	fillProfInfo(cooking, P.ProfInfo.Cooking)
	fillProfInfo(firstAid, P.ProfInfo.FirstAid)
	
	P.RidingSkill = m:GetRidingSkill()
	P.IsSwimming = IsSwimming()
end

function m:mapUpdate()
	m.Map.Continent = GetCurrentMapContinent()
	m.Map.ZoneID = GetCurrentMapAreaID()
end

function m:NextMount()
	return MiniMount_Mounts["Next"..P.Faction.."Mount"]
end


function m:getCanFly()
	--MiniMount_ZoneSpeeds
	local content = m.Map.Continent
	local zone = m.Map.ZoneID
	
	if (P.RidingSkill<m.RIDINGSKILLLVL_EXPERT) then
		m.CanFly = nil
	else
		local FML_Name = GetSpellInfo(m.RIDINGSPELLID_FML)
		local CWF_Name = GetSpellInfo(m.RIDINGSPELLID_CWF)
		local WFW_Name = GetSpellInfo(m.RIDINGSPELLID_WFW)
		local CSR_Name = GetSpellInfo(m.RIDINGSPELLID_CSR)

		local Test_FML = GetSpellInfo(FML_Name)
		local Test_CWF = GetSpellInfo(CWF_Name)
		local Test_WFW = GetSpellInfo(WFW_Name)
		local Test_CSR = GetSpellInfo(CSR_Name)

		if (Test_FML ~= nil) and ((content == m.CONTINENT_EASTERNKINGDOMS) or (content == m.CONTINENT_KALIMDOR) or (content == m.CONTINENT_DEEPHOLM)) then
			m.CanFly = IsFlyableArea()
		elseif (P.RidingSkill >= m.RIDINGSKILLLVL_EXPERT) and (content == m.CONTINENT_OUTLAND) then
			m.CanFly = IsFlyableArea()
		elseif (Test_CWF ~= nil) and (content == m.CONTINENT_NORTHREND) then
			local _,_,wgactive = GetWorldPVPAreaInfo(1)	-- Wintergrasp
			if (zone == z.WINTERGRASP) and (wgactive == nil) then
				m.CanFly = nil
			else
				m.CanFly = IsFlyableArea()
			end
		elseif (Test_WFW ~= nil) and (content == m.CONTINENT_PANDARIA) then
			m.CanFly = IsFlyableArea()
		else
			m.CanFly = nil
		end
	end
	MiniMount:Msg("CanFly: "..tostring(m.CanFly), "debug", 2)
	return m.CanFly
end

-- If override is set, force a map update.
function m:SetMapCurrent(override)
	if (not WorldMapFrame:IsVisible()) or (override) then
		SetMapToCurrentZone()
		m:mapUpdate()
	end
	m:getCanFly()
end

-- Do Macro Preperations for Classes (If possible)
function m:MountPrepare()
	local _, Class = UnitClass("player")

	-- Remove a Druid's Shapeshift
	if Class == "DRUID" then
		return "\n/cancelform [btn:1]"
	-- Remove a Shaman's Shapeshift
	elseif Class == "SHAMAN" then
		return "\n/cancelform [btn:1]"
	end

	return ""
end

-- Set Holiday Info
function m:SetHoliday()
	MiniMount:Msg("Setting Holiday...", "debug", 1)
	local _, month, day, year = CalendarGetDate()
	local cmonth = CalendarGetMonth()
	local moffset = cmonth-month
	local nume = CalendarGetNumDayEvents(moffset,day)
	for x=1,nume do
		local Name, _, _, etype, status , _, graphic = CalendarGetDayEvent(moffset, day, x)
		if etype == "HOLIDAY" then
			MiniMount:Msg("Detected Holiday: \""..tostring(Name).."\"", "debug", 1)
			if (graphic == "Calendar_LoveInTheAir") then
				m.Holiday = "Love is in the Air"
			elseif (graphic == "Calendar_Noblegarden") then
				m.Holiday = "Noblegarden"
			elseif (graphic == "Calendar_Brewfest") then
				m.Holiday = "Brewfest"
			elseif (graphic == "Calendar_HallowsEnd") then
				m.Holiday = "Hallows End"
			elseif (graphic == "Calendar_WinterVeil") then
				m.Holiday = "Feast of Winter Veil"
			end
		end
	end
	MiniMount:Msg("Final Holiday Setting: "..tostring(m.Holiday), "debug", 1)
end

-- Timer function
function m:Timer(name,targettime,functiontodo, ...)
	LibKJ:Timer(m.MINIMOUNT_TITLE, name, targettime, functiontodo, ...)
end

-- Check for zones you can't fly in, even if IsFlyableArea() says you can.
function m:ZoneFlyingCheck()
	local rval = false
	rval = (m.Map.ZoneID ~= m.ZoneIDs.HEARTOFAZEROTH
		and m.Map.ZoneID ~= m.ZoneIDs.HELHEIM
		and m.Map.ZoneID ~= m.ZoneIDs.THEFELHAMMER)
		
	return rval
end

function m:RemoveFromTable(intable, keytable)
	local outTable = {}
	if not keytable or type(keytable) ~= "table" or #keytable == 0 then
		MiniMount:Msg("keytable empty. nothing Removed.", "debug", 1)
		return intable
	end
	
	for k,v in pairs(intable) do
		local keyfound = false
		for i,key in pairs(keytable) do
			if k == key then
				MiniMount:Msg(format("k: %s, key: %s", tostring(k), tostring(key)), "debug", 3)
				keyfound = true
			end
		end
		if keyfound == false then
			outTable[k] = v
		end
	end
	
	return outTable
end

function m:CheckHolidayMounts()
	if m.Holiday == nil then m:SetHoliday() end
	if m.Holiday == nil then return end
	if MiniMount_Options.Global.UseHolidayMounts == false then return end
	
	-- Check to see if we have a holiday mount in our current collection
	local hasHolidayMount = nil
	for mID,md in pairs(m.CurrentDB) do
		if md.holiday and md.holiday == m.Holiday then
			hasHolidayMount = true
		end
	end
	if not hasHolidayMount then return end
	
	local remdata = {}
	for mID,md in pairs(m.CurrentDB) do
		if not md.holiday or md.holiday ~= m.Holiday then
			MiniMount:Msg(format("Removing mount %s (id: %s). Reason: Not a Holiday Mount for %s",tostring(md.name), tostring(md.mountID), tostring(m.Holiday)), "debug", 1)
			if not tContains(remdata, mID) then
				tinsert(remdata, mID)
			end
		end
	end
	
	if #remdata > 0 then
		m.CurrentDB = m:RemoveFromTable(m.CurrentDB,remKeys)
	end
end

function m:UpdateMountTypes(desired)
	local types = m:GetMountTypes(desired)
	if m.CurrentTypes == types then
		return types, false
	end
	m.CurrentTypes = types
	return types, true
end

function m:GetMountTypes(DesiredType)
	if not DesiredType then DesiredType = m.TYPE_AUTO end
	--print("Get MountType: "..tostring(DesiredType))
	m:GetPlayerData()	-- Make sure we're up to date...
	m:mapUpdate()
	if C_MountJournal.GetNumMounts() <= 1 then return nil end
	
	--[[
	230 for most ground mounts
	231 for [Riding Turtle] and  [Sea Turtle]
	232 for [Vashj'ir Seahorse] (was named Abyssal Seahorse prior to Warlords of Draenor)
	241 for Blue, Green, Red, and Yellow Qiraji Battle Tank (restricted to use inside Temple of Ahn'Qiraj)
	242 for Swift Spectral Gryphon (hidden in the mount journal, used while dead in certain zones)
	247 for Red Flying Cloud
	248 for most flying mounts, including those that change capability based on riding skill
	254 for Subdued Seahorse, Brinedeep Bottom-Feeder and  [Fathom Dweller]
	269 for Azure and Crimson Water Strider
	284 for Chauffeured Mekgineer's Chopper and Chauffeured Mechano-Hog
	]]
	
	local mTypes = { 230 } -- Default to Ground Type
	if (MiniMount_Mounts.MountList.Common["mt269"] or MiniMount_Mounts.MountList[P.Faction]["mt269"]) then
		mTypes = { 230, 269 }
	end
	if DesiredType == m.TYPE_AUTO and MiniMount_Options.Global.UseFlyingWithGround == true then
		mTypes = { 230, 248, 247 }
		if (MiniMount_Mounts.MountList.Common["mt269"] or MiniMount_Mounts.MountList[P.Faction]["mt269"]) then
			mTypes = { 230, 248, 247, 269 }
		end
	end
	if P.Level < 20 or P.RidingSkill == 1 then
		if MiniMount_Mounts.MountList.Common["mt284"] or MiniMount_Mounts.MountList[P.Faction]["mt284"] then
			mTypes = { 284 }
		else
			return nil
		end
	elseif m.Map.ZoneID == m.ZoneIDs.AHNQIRAJ40 then
		if MiniMount_Mounts.MountList.Common["mt241"] then
			mTypes = { 241 }
		end
	elseif P.IsSwimming and (MiniMount_Mounts.MountList.Common["mt232"] or MiniMount_Mounts.MountList[P.Faction]["mt232"]) and (m.Map.ZoneID == m.ZoneIDs.KELPTHARFOREST or m.Map.ZoneID == m.ZoneIDs.ABYSSALDEPTHS or m.Map.ZoneID == m.ZoneIDs.SHIMMERINGEXPANSE) then
		mTypes = { 232 }
	elseif P.IsSwimming and (MiniMount_Mounts.MountList.Common["mt254"] or MiniMount_Mounts.MountList[P.Faction]["mt254"]) then
		mTypes = { 254 }
	elseif P.IsSwimming and (MiniMount_Mounts.MountList.Common["mt231"] or MiniMount_Mounts.MountList[P.Faction]["mt231"]) then
		mTypes = { 231 }
	elseif (IsFlyableArea() and m:ZoneFlyingCheck() and DesiredType == m.TYPE_AUTO) or DesiredType == m.TYPE_FLYING then
		mTypes = { 248, 247 }
	end
	
	return mTypes
end

-- Chooses a new Mount
function m:SelectNextMount(mountType, force)
	--print("MountType: "..tostring(mountType))
	if not mountType then mountType = m.TYPE_AUTO end
	m.CurrentCommandMode = mountType
	local mTypes, isNewType = m:UpdateMountTypes(m.CurrentCommandMode)
	if mTypes == nil then
		MiniMount:Msg("No Mount Types found.", "error")
		return nil
	end
	local DBCount = 0
	for k,v in pairs(m.CurrentDB) do
		DBCount = DBCount + 1
	end
	
	--MiniMount:Msg(format("Current DB count: %s",tostring(DBCount)), "debug", 1)
	if DBCount == 0 or force or isNewType == true then
		-- Collect and merge our stored mount databases, based on the Mount Types we got earlier.
		local DB = {}
		for k,mType in pairs(mTypes) do
			local CommonDB = MiniMount_Mounts.MountList.Common["mt"..mType]
			local FactionDB = MiniMount_Mounts.MountList[P.Faction]["mt"..mType]
			DB = m:mergeTables(DB, CommonDB)
			DB = m:mergeTables(DB, FactionDB)
		end
		
		-- Remove mounts, based on various factors, including Holidays and profession requirements
		local remKeys = {}
		local holidaymountfound = nil
		for id,mountInfo in pairs(DB) do
			if mountInfo.holiday and mountInfo.holiday == m.Holiday then
				holidaymountfound = true
				break
			end
		end
		
		for id,mountInfo in pairs(DB) do
			local rem = false
			-- Class
			if mountInfo.class then
				MiniMount:Msg(format("Class: %s VS %s",tostring(P.Class), tostring(mountInfo.class)), "debug", 1)
			end
			if mountInfo.class and mountInfo.class ~= P.Class then
				MiniMount:Msg(format("Removing mount %s (id: %s). Reason: Invalid Class",tostring(mountInfo.name), tostring(mountInfo.mountID)), "debug", 1)
				rem = true
			end
			
			-- Professions
			if mountInfo.prof then
				MiniMount:Msg(format("Prof1: %s, Prof2: %s",tostring(P.ProfInfo.Prof1.Name), tostring(P.ProfInfo.Prof2.Name)), "debug", 1)
				local profN = mountInfo.prof
				local profS = mountInfo.proflevel
				local pr1 = (tostring(P.ProfInfo.Prof1.Name) == profN)
				local pr2 = (tostring(P.ProfInfo.Prof2.Name) == profN)
				MiniMount:Msg(format("tostring(P.ProfInfo.Prof1.Name) == profN: %s",tostring(pr1)), "debug", 2)
				MiniMount:Msg(format("tostring(P.ProfInfo.Prof2.Name) == profN: %s",tostring(pr2)), "debug", 2)
				MiniMount:Msg(format("Prof for mount %s: %s, skill: %s",tostring(mountInfo.name), tostring(profN), tostring(profS)), "debug", 2)
				if (not P.ProfInfo.Prof1.Name) and (not P.ProfInfo.Prof2.Name) then
					MiniMount:Msg(format("Removing mount %s (id: %s). Reason: No Professions.",tostring(mountInfo.name), tostring(mountInfo.mountID)), "debug", 3)
					rem = true
				else
					MiniMount:Msg(format("Removing mount %s (id: %s). Reason: No matching Professions.",tostring(mountInfo.name), tostring(mountInfo.mountID)), "debug", 3)
					rem = true
				end
				if (tostring(P.ProfInfo.Prof1.Name) == profN) and (tonumber(P.ProfInfo.Prof1.Level) >= profS) then
					MiniMount:Msg(format("Restoring mount %s (id: %s). Reason: Profession matches and skill is high enough.",tostring(mountInfo.name), tostring(mountInfo.mountID)), "debug", 3)
					rem = false
				end
				if (tostring(P.ProfInfo.Prof2.Name) == profN) and (tonumber(P.ProfInfo.Prof2.Level) >= profS) then
					MiniMount:Msg(format("Restoring mount %s (id: %s). Reason: Profession matches and skill is high enough.",tostring(mountInfo.name), tostring(mountInfo.mountID)), "debug", 3)
					rem = false
				end
			end
			
			-- Non-Holiday mounts, during Holidays
			if MiniMount_Options.Global.UseHolidayMounts == true and holidaymountfound and (not mountInfo.holiday or mountInfo.holiday ~= m.Holiday) then
				MiniMount:Msg(format("Removing mount %s (id: %s). Reason: Not a Holiday Mount for %s",tostring(mountInfo.name), tostring(mountInfo.mountID), tostring(m.Holiday)), "debug", 3)
				rem = true
			end

			if rem == true then
				tinsert(remKeys,id)
			end
		end
		
		DB = m:RemoveFromTable(DB,remKeys)
		m.CurrentDB = DB
	end
	
	m:CheckHolidayMounts()
	
	local keyset = {}
	for k in pairs(m.CurrentDB) do
		tinsert(keyset, k)
	end
	if #keyset == 0 and force then
		MiniMount:Msg("Still No Mount Types found.", "error")
		return nil
	elseif #keyset == 0 then
		m:FillMountDatabase()
		return m:SelectNextMount(m.CurrentCommandMode, true)
	end
	
	MiniMount:Msg(format("Max Rand Number: %s",tostring(#keyset)), "debug", 1)
	local rand = random(#keyset)
	MiniMount:Msg(format("Random Value: %s",tostring(rand)), "debug", 1)
	local mount = m.CurrentDB[keyset[rand]]
	MiniMount_Mounts["Next"..P.Faction.."Mount"] = mount
	return mount
end

function m:mergeTables(MergeTo, MergeFrom)
	if MergeFrom == nil then return MergeTo end
	for k,v in pairs (MergeFrom) do
		if (MergeTo[k]) and type(MergeTo[k]) == "table" and type(v) == "table" then
			m:mergeTables(MergeTo[k], v)
		else
			MergeTo[k] = v
		end
	end
	return MergeTo
end

function m:BuildMountDatabase()
	if (not MiniMount_Mounts) then
		MiniMount_Mounts = {}
		MiniMount_Mounts = m.Defaults["MountDB"]
	end
	if not (MiniMount_Mounts["MountList"]["Common"]) then
		MiniMount_Mounts["MountList"]["Common"] = {}
	end
	if not (MiniMount_Mounts["MountList"][P.Faction]) then
		MiniMount_Mounts["MountList"][P.Faction] = {}
	end
end

function m:FillMountDatabase(doWipe)
	if doWipe and doWipe == true then
		MiniMount:Msg("Wiping the MountList before refilling it.", "debug", 1)
		MiniMount_Mounts["MountList"] = {}
		MiniMount:Msg(format("MountList Count: %s", tostring(#MiniMount_Mounts["MountList"])), "debug", 1)
	end
	local mountIDs =  C_MountJournal.GetMountIDs()
	if (not MiniMount_Mounts["MountList"]) or (not MiniMount_Mounts["MountList"]["Common"]) or (not MiniMount_Mounts["MountList"][P.Faction]) then
		m:BuildMountDatabase()
	end
	for k, mID in pairs(mountIDs) do
		local creatureName, spellID, icon, active, isUsable, sourceType, isFavorite, isFactionSpecific, faction, hideOnChar, isCollected, mountID = C_MountJournal.GetMountInfoByID(mID)
		local creatureDisplayID, descriptionText, sourceText, isSelfMount, mountType = C_MountJournal.GetMountInfoExtraByID(mID)
		strfaction = "Common"
		if (isFactionSpecific and faction == 0) then strfaction = "Horde" end
		if (isFactionSpecific and faction == 1) then strfaction = "Alliance" end
		if mountType ~= nil then
			strmountType = "mt"..tostring(mountType)
			if (isCollected) then
				data = {
					["name"] = creatureName,
					["spellID"] = spellID,
					["mountID"] = mountID,
					["mountType"] = mountType,
					["selfmount"] = isSelfMount
				}
				if m.MountAdditiveDB[mountID] then
					data = m:mergeTables(data, m.MountAdditiveDB[mountID])
				end
				if not MiniMount_Mounts["MountList"][strfaction] then
					MiniMount_Mounts["MountList"][strfaction] = {}
				end
				if not MiniMount_Mounts["MountList"][strfaction][strmountType] then 
					MiniMount_Mounts["MountList"][strfaction][strmountType] = {}
				end
				if not MiniMount_Mounts["MountList"][strfaction][strmountType][mID] then
					MiniMount:Msg(format("Adding Mount to DB: %s for %s", tostring(data.name), strfaction), "debug", 2)
					MiniMount_Mounts["MountList"][strfaction][strmountType][mID] = data
				end
			end
		end
	end
end