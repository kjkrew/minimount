--[[
	MiniMount
	English Localization File
	Version <%version%>
	
	Revision: $Id: enUS.lua 7 2016-09-27 11:26:35 Pacific Daylight Time Kjasi $
	
	This file affects non-critical (graphic) localizations only.
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
local L = m.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Localization.|r")
	return
end

L["Version"] = "Version %s"
L["None"] = "None"

L["MiniMount is Loaded"] = "Version %s is now loaded!"

L["Macro Created"] = "A MiniMount Macro has been created for you! This will always update and show you the next mount that will be summoned!"

-- Error Messages
L["Error Wrong Option"] = "Incorrect Variables submitted. Operation Failed."
L["Error Level Not High Enough"] = "You are not a high enough level to have a mount."
L["Error Skill Not High Enough"] = "Your riding skill isn't high enough to ride a mount."
L["Error Skill Not High Enough to Fly"] = "Your riding skill isn't high enough to ride a flying mount. We are giving you a ground mount instead."
L["Error Mount Database Empty"] = "An error occured while generating the mount database. Operation aborted."
L["Error No Fly Zone"] = "You can not fly here. Aborting."

L["Error No Mounts Any"] = "No mounts found. You might want to go learn some!"
L["Error No Mounts Usable"] = "No usable mounts found. Please try again."

L["Error Unknown Mounts"] = "You have one or more mounts that are not in our database! Please send a message to the author of MiniMount, saying if the mounts are flyers, or land mounts. Also include the following information:"
L["Mount Report"] = "This Mount was not found: %s"

-- Passed Strings: L["commandline ground"], L["commandline fly"], L["commandline random"]
L["Help Base"] = "MiniMount supports the following arguments:\n(none) = Automatically selects a mount for you.\n%s = Selects a ground mount for you.\n%s = Selects a flying mount for you.\n%s = Selects a random mount."

--==Mount Types & Speeds==--
L["Type Shifting"] = "Adaptive"
L["Type Flying"] = "Flying"
L["Type Ground"] = "Ground"
L["Type Ground Slow"] = "Slow Ground"
L["Type Swimming"] = "Swimming"
L["Speed Adaptive"] = "Adaptive"
L["Usable Anywhere"] = "Anywhere"

L["Display List Array"] = "%s, %s"
L["Display Speed Dash"] = "%s-%s"

--==Interface Text==--
L["Random Mount Title"] = "Random Mount"
L["Random Mount Tooltip"] = "MiniMount will randomly select a mount for you to use, instead of a specific mount."

-- Holiday Mounts
L["Use Holiday Mounts Title"] = "Holiday Mounts Only, during Holidays"
L["Use Holiday Mounts Tooltip"] = "If enabled, during Holiday events, MiniMount will ONLY use holiday mounts, if you have the mount."
-- Dismounting While Flying
L["Dismount While Flying Title"] = "Allow Dismounting While Flying"
L["Dismount While Flying Tooltip"] = "Enabling this will allow you to Dismount while flying."
-- Include Flying with Ground Mounts
L["Include Flying with Ground Title"] = "Include Flying with Ground Mounts"
L["Include Flying with Ground Tooltip"] = "When enabled, summoning a ground mount will also give a chance to include flying mounts."

--==Command-Line items==--
L["Command Depreciated"] = "This command has been deprecated and will be removed soon. Type /minimount %s for a list of commands."

--==SpellPage==--
L["Mount List Display Data"] = "Type: %s\nSpeed: %s\nSeats: %i\nUsable: %s"
L["Mount List Display Data Holiday"] = "\nHoliday: %s"	-- Attached to L["Mount List Display Data"] if it has a holiday association.