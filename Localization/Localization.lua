﻿--[[
	MiniMount
	Primary Localization File
	Version <%version%>
	
	Revision: $Id: Localization.lua 24 2016-09-27 11:19:25 Pacific Daylight Time Kjasi $
	
	This file affects critical functions of MiniMount. Without these Localizations, MiniMount will not work properly.
	
	-- NOTE: Some items in this file (such as the commandline options) were translated by Google. Please check them to see if they are correct!
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
m.Localize = {}
local L = m.Localize


-- [[ enUS & enGB (English) ]] --

L["Message Normal"] = "%s:|r %s"					-- MiniMount title, then Message = "MiniMount: Message"
L["Message Error"] = "%s Error:|r %s"				-- MiniMount title, then Message = "MiniMount Error: Error Message"
L["Message Debug"] = "%s Debug Message:|r %s"		-- MiniMount title, then Message = "MiniMount Debug Message: Debug Message"


--== Command-line Functions ==--
L["commandline options"] = "options"
L["commandline new"] = "new"
L["commandline random"] = "random"
L["commandline ground"] = "ground"
L["commandline fly"] = "fly"
L["commandline swim"] = "swim"
L["commandline swim regular"] = "swim reg"
L["commandline rescan"] = "rescan"
L["commandline help"] = "help"


-- [[ deDE (German) ]] --
if (GetLocale() == "deDE") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s Fehler:|r %s"
	L["Message Debug"] = "%s Debugmeldung:|r %s"

	--== Command-line Functions ==--
	L["commandline options"] = "optionen"
	L["commandline new"] = "neu"
	L["commandline random"] = "zufällig"
	L["commandline ground"] = "boden"
	L["commandline fly"] = "fliegen"
	L["commandline swim"] = "schwimmen"
	L["commandline swim regular"] = "schwimmen reg"
	L["commandline rescan"] = "rescan"
	L["commandline help"] = "hilfe"
	
	-- [[ frFR (French) ]] --
elseif (GetLocale() == "frFR") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Erreur %s:|r %s"
	L["Message Debug"] = "Déboguer %s:|r %s"

	--== Command-line Functions ==--
	L["commandline options"] = "options"
	L["commandline new"] = "nouveau"
	L["commandline random"] = "aléatoire"
	L["commandline ground"] = "terre"
	L["commandline fly"] = "volant"
	L["commandline swim"] = "nager"
	L["commandline swim regular"] = "nager rég"
	L["commandline rescan"] = "réanalyser"
	L["commandline help"] = "aider"
	
	-- [[ esES (Spanish) & esMX (South-American Spanish) ]] --
elseif (GetLocale() == "esES") or (GetLocale() == "esMX") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Error %s:|r %s"
	L["Message Debug"] = "Mensaje de depuración %s:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "opciones"
	L["commandline new"] = "nuevo"
	L["commandline random"] = "azar"
	L["commandline ground"] = "tierra"
	L["commandline fly"] = "volar"
	L["commandline swim"] = "nadar"
	L["commandline swim regular"] = "nadar regularmente"
	L["commandline rescan"] = "reescanear"
	L["commandline help"] = "ayudar"

	-- [[ itIT (Italian) ]] --
elseif (GetLocale() == "itIT") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Errore  %s:|r %s"
	L["Message Debug"] = "di debug %s:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "opzioni"
	L["commandline new"] = "nuovo"
	L["commandline random"] = "casuale"
	L["commandline ground"] = "terra"
	L["commandline fly"] = "volare"
	L["commandline swim"] = "nuotare"
	L["commandline swim regular"] = "nuotare regolare"
	L["commandline rescan"] = "ripetere l'analisi"
	L["commandline help"] = "aiuto"
	
	-- [[ ptPT (Portuguese) & ptBR (Brazilian Portuguese) ]] --
elseif (GetLocale() == "ptBR") or (GetLocale() == "ptPT") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "Erro %s:|r %s"
	L["Message Debug"] = "Debug %s:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "opções"
	L["commandline new"] = "novo"
	L["commandline random"] = "casual"
	L["commandline ground"] = "solo"
	L["commandline fly"] = "voar"
	L["commandline swim"] = "nadar"
	L["commandline swim regular"] = "nadar regularmente"
	L["commandline rescan"] = "redigitalize"
	L["commandline help"] = "ajudar"
	
	-- [[ ruRU (Russian) ]] --
elseif (GetLocale() == "ruRU") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s ошибке:|r %s"
	L["Message Debug"] = "%s отладки:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "опции"
	L["commandline new"] = "новый"
	L["commandline random"] = "произвольный"
	L["commandline ground"] = "земля"
	L["commandline fly"] = "летать"
	L["commandline swim"] = "плавать"
	L["commandline swim regular"] = "плавать регулярное"
	L["commandline rescan"] = "пересканировать"
	L["commandline help"] = "помощь"
	
	-- [[ koKR (Korean) ]] --
elseif (GetLocale() == "koKR") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s 오류:|r %s"
	L["Message Debug"] = "%s 디버그:|r %s"

	--== Command-line Functions ==--
	L["commandline options"] = "옵션"
	L["commandline new"] = "새로운"
	L["commandline random"] = "무작위"
	L["commandline ground"] = "흙"
	L["commandline fly"] = "비행하다"
	L["commandline swim"] = "헤엄"
	L["commandline swim regular"] = "정기적으로 수영"
	L["commandline rescan"] = "다시 검색"
	L["commandline help"] = "도움"
	
	-- [[ zhCN (Chinese, Simplified) ]] --
elseif (GetLocale() == "zhCN") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s错误:|r %s"
	L["Message Debug"] = "%s调试:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "选项"
	L["commandline new"] = "新"
	L["commandline random"] = "随便"
	L["commandline ground"] = "地"
	L["commandline fly"] = "翔"
	L["commandline swim"] = "游泳"
	L["commandline swim regular"] = "游泳定期"
	L["commandline rescan"] = "重新扫描"
	L["commandline help"] = "助"
	
	-- [[ zhTW (Chinese, Traditional) ]] --
elseif (GetLocale() == "zhTW") then
	L["Message Normal"] = "%s:|r %s"
	L["Message Error"] = "%s錯誤:|r %s"
	L["Message Debug"] = "%s調試:|r %s"
	
	--== Command-line Functions ==--
	L["commandline options"] = "選項"
	L["commandline new"] = "新"
	L["commandline random"] = "隨便"
	L["commandline ground"] = "地"
	L["commandline fly"] = "翔"
	L["commandline swim"] = "游泳"
	L["commandline swim regular"] = "游泳定期"
	L["commandline rescan"] = "重新掃描"
	L["commandline help"] = "助"

end