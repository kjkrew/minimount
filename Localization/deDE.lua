--[[
	MiniMount
	German Localization File
	Version <%version%>
	
	Revision: $Id: deDE.lua 8 2012-12-23 12:06:19 PST Kjasi $
	
	This file affects non-critical (graphic) localizations only.
	
	-- NOTE: Some items in this file were translated by Google. Please check them to see if they are correct!
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
local L = m.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Localization.|r")
	return
end

if (GetLocale() == "deDE") then

L["Version"] = "Version %s"
L["None"] = "Keins"

L["MiniMount is Loaded"] = "Version %s wurde geladen!"

L["Macro Created"] = "Ein MiniMount Makro wurde erstellt! Das Makro wird automatisch aktualisiert und zeigt das n�chste Reittier an, das gerufen wird."

-- Error Messages
L["Error Wrong Option"] = "Fehlerhafte Variablns �bertragen. Operation fehlgeschlagen."
L["Error Level Not High Enough"] = "Dein Level ist nicht hoch genug um ein Reittier zu besitzen."
L["Error Skill Not High Enough"] = "Deine Reitf�higkeit ist nicht hoch genug um reiten zu k�nnen."
L["Error Skill Not High Enough to Fly"] = "Deine Reitf�higkeit ist nicht hoch genug um ein Flugreittier reiten zu k�nnen. Es wird ein normales Reittier statt dessen gerufen."
L["Error Mount Database Empty"] = "Bei der Erstellung der Reittier-Datenbank ist ein Fehler aufgetreten. Operation abgebrochen."
L["Error No Fly Zone"] = "Du kannst hier nicht fliegen. Breche ab."

L["Error No Mounts Any"] = "Keine Reittiere gefunden. Geh und kauf dir ein Reittier!"
L["Error No Mounts Usable"] = "Kein benutzbares Reittier gefunden. Bitte versuche es erneut."

L["Error Unknown Mounts"] = "Du besitzt ein oder mehrere Reittiere die nicht in der Datenbank vorhanden sind. Bitte informiere den Autor von MiniMount und teile ihm mit ob es sich um ein Flugtier, oder ein Reittier handelt. Bitte gib auch die folgenden Informationen an:"
L["Mount Report"] = "Dieses Reittier wurde gefunden: %s"

-- All items after this line were translated with Google. They may need some corrections.

-- Passed Strings: MINIMOUNT_CMD_GROUND, MINIMOUNT_CMD_FLY, MINIMOUNT_CMD_RANDOM
L["Help Base"] = "MiniMount unterst�tzt die folgenden Argumente:\n(none) = W�hlt automatisch eine Halterung f�r Sie.\n%s = W�hlt einen Boden montieren f�r Sie.\n%s = W�hlt ein fliegendes Reittier f�r Sie.\n%s = W�hlt eine zuf�llige montieren."

--==Interface Text==--
L["Random Mount Title"] = "Zuf�llige Reittier"
L["Random Mount Tooltip"] = "MiniMount werden nach dem Zufallsprinzip w�hlen Sie ein reittier\nf�r Sie zu nutzen, anstatt ein bestimmtes reittier."

-- Holiday Mounts
L["Use Holiday Mounts Title"] = "Urlaub reittier Nur w�hrend der Ferienzeit"
L["Use Holiday Mounts Tooltip"] = "Wenn aktiviert, w�hrend der Ferien Veranstaltungen wird MiniMount NUR verwenden Urlaub reittier, wenn Sie die reittier haben."
-- Dismounting While Flying
L["Dismount While Flying Title"] = "Lassen Demontage beim Fliegen"
L["Dismount While Flying Tooltip"] = "Die Aktivierung dieser erm�glicht es Ihnen, abzusteigen w�hrend des Fluges."

-- Perfered Mounts
L["Perfered Mount Ground Title"] = "Bevorzugte Boden Reittier"
L["Perfered Mount Flying Title"] = "Bevorzugte fliegenden Reittier"
L["Perfered Mount Ground Tooltip"] = "Die reittier Sie wollen immer zeigen, sich f�r Boden Reittier."
L["Perfered Mount Flying Tooltip"] = "Die reittier Sie wollen immer zeigen sich f�r die Fliegerei Reittier."

--==Command-Line items==--
-- Passed Variable: MINIMOUNT_CMD_HELP
L["Command Depreciated"] = "Dieser Befehl ist veraltet und wird in K�rze entfernt werden. Typ /minimount %s f�r eine Liste von Befehlen."

L["Mount List Display Data"] = "Typ: %s/nGeschwindigkeit: %s/nSitzpl�tze: %i/nVerwendbar: %s"
L["Mount List Display Data Holiday"] = "\nFesttag: %s"	-- Attached to L["Mount List Display Data"] if it has a holiday association.

end