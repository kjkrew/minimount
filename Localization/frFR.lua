--[[
	MiniMount
	French Localization File
	Version <%version%>
	
	Revision: $Id: frFR.lua 3 2012-12-23 12:06:54 PST Kjasi $
	
	This file affects non-critical (graphic) localizations only.
	
	-- NOTE: All items in this file were translated by Google. Please check them to see if they are correct!
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
local L = m.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Localization.|r")
	return
end

if (GetLocale() == "frFR") then

L["Version"] = "Version %s"
L["None"] = "aucun"

L["MiniMount is Loaded"] = "La version %s est maintenant charg�!"

L["Macro Created"] = "Une Macro MiniMount a �t� cr�� pour vous! Ce sera toujours � jour et de vous montrer le montage suivant qui sera convoqu�!"

-- Error Messages
L["Error Wrong Option"] = "Les variables incorrectes soumis. Echec de l'op�ration."
L["Error Level Not High Enough"] = "Vous n'�tes pas un niveau suffisamment �lev� pour avoir une monture."
L["Error Skill Not High Enough"] = "Votre comp�tence de monte n'est pas assez �lev� pour faire du montage."
L["Error Skill Not High Enough to Fly"] = "Votre comp�tence de monte n'est pas assez �lev� pour monter une monture volante. Nous vous donnons une monture terrestre � la place."
L["Error Mount Database Empty"] = "Une erreur s'est produite lors de la g�n�ration de la base de donn�es de montage. Op�ration annul�e."
L["Error No Fly Zone"] = "Vous ne pouvez pas voler ici. Abandon."

L["Error No Mounts Any"] = "Pas de supports trouv�s. Vous pouvez aller apprendre un peu!"
L["Error No Mounts Usable"] = "Pas de supports utilisables trouv�. S'il vous pla�t essayez de nouveau."

L["Error Unknown Mounts"] = "Vous avez un ou plusieurs supports qui ne sont pas dans notre base de donn�es! S'il vous pla�t envoyer un message � l'auteur de MiniMount, disant que si les supports sont circulaires, ou des montures terrestres. �galement inclure les informations suivantes:"
L["Mount Report"] = "Ce mont est introuvable: %s"

-- Passed Strings: MINIMOUNT_CMD_GROUND, MINIMOUNT_CMD_FLY, MINIMOUNT_CMD_RANDOM
L["Help Base"] = "MiniMount appuie les arguments suivants:\n(aucun) = s�lectionne automatiquement une monture pour vous.\n%s= S�lectionne une monture terrestre pour vous.\n%s = S�lectionne une monture volante pour vous.\n%s = S�lectionne une monture al�atoire."

--==Interface Text==--
L["Random Mount Title"] = "Mont al�atoire"
L["Random Mount Tooltip"] = "MiniMount choisira au hasard un montage pour vous d'utiliser, au lieu d'un montage sp�cifique."

-- Holiday Mounts
L["Use Holiday Mounts Title"] = "Location de Supports Seulement, pendant les vacances"
L["Use Holiday Mounts Tooltip"] = "Si elle est activ�e, lors des �v�nements de vacances, MiniMount SEULEMENT utiliser des montures de vacances, si vous avez la monture."
-- Dismounting While Flying
L["Dismount While Flying Title"] = "Laissez D�montage en vol"
L["Dismount While Flying Tooltip"] = "Activer cette option vous permet de d�montage en vol."

-- Perfered Mounts
L["Perfered Mount Ground Title"] = "Terre pr�f�r� mont"
L["Perfered Mount Flying Title"] = "Pr�f�r� Mont Vol"
L["Perfered Mount Ground Tooltip"] = "Le montage que vous voulez toujours de se pr�senter pour les montures terrestres."
L["Perfered Mount Flying Tooltip"] = "Le montage que vous voulez toujours de se pr�senter pour les montures volantes."

--==Command-Line items==--
-- Passed Variable: MINIMOUNT_CMD_HELP
L["Command Depreciated"] = "Cette commande est obsol�te et sera supprim�e prochainement. Type /minimount %s pour une liste des commandes."

L["Mount List Display Data"] = "Type: %s/nVitesse: %s/nSi�ges: %i/nUtilisable: %s"
L["Mount List Display Data Holiday"] = "\nf�te: %s"	-- Attached to L["Mount List Display Data"] if it has a holiday association.

end