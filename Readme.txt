MiniMount v<%version%>

A simple addon to choose a random mount, based on location and skill.

-= Command-line List =-
/minimount
/mount
/mm
	(No Further Commands) = Automatically choose your highest level mounts, and randomly choose one.
	help = Command List
	new or newmount or new mount = Choose a new mount for your next mount.
	ground = Choose the best ground-only mount you can.
	fly = Choose only random flying mounts.
	random = Choose a totally random mount from any of the mounts you have.

-= Update History =-

v<%version%>
 - TOC Update for 7.1
 
v2.0.0
 - Updated for Legion!
 - Completely rewritten to no longer use an internal database. All mounts (even new ones) are now automatically available for use! Please delete your old MiniMount installation and variables before running, as everything has changed!
 - Better swimming mount selection!
 - Waterstriders are now considered Ground Mounts.
 - Added command "rescan" to force MiniMount to rebuild the list of your available mounts.
 
v1.11.0
 - TOC Update for 6.1
 - Updated interface for the Collections window.

v1.10.2
 - Functional Update for 6.0 (No mounts added to the database.)

v1.10.1
 - TOC update for 5.1
 - Added Zen Flight and 5.1 mounts to the database.

v1.10.0
 - Updated for Mists of Pandaria!
 - Added 5.0.1 mounts.
 - Rewrote a lot of functions from the ground up!
 - New database format allows us to support a lot more options and choices!
 - Now fully supports French (frFR) client! Please help with correcting any localization errors!
 - Added preliminary support for esES, esMX, ruRU, zhCN, zhTW and ptBR clients. Please help with correcting any localization errors!
 - The MiniMount Macro now supports using items and spells that perform mount-like abilities. IE: Worgen's Running Wild ability, Druid's Flight Forms, and the Magic Brooms.
 - Fixed a lot of long-standing bugs with time/place-specific mounts.

v1.9.3
 - Added 4.3 PTR mounts.
 - Alt-Right-Clicking the MiniMount Macro will now call a flying mount.
 - Copied some flying mounts into the ground mounts, as those flying mounts can also be ground mounts.
 - Added "rescan" command, that will clear and rescan all your mounts. Use this if MiniMount's next mount isn't the proper one.
 - Added a function to the Macro that will let it use Broomsticks during Hallow's Eve!
 - Fixed a holiday mount bug where typing /mm fly during a holiday that only has ground mounts, would summon a holiday ground mount instead of a flying mount.
 - Fixed a bug with 4.x and Wintergrasp

v1.9.2
 - Updated for 4.2 (Thanks to Myrmidon!)
 - Added Winged Guardian to the database (Thanks to Myrmidon!)
 - Added several ground, flying & swimming mounts.
 - Special Thanks to Myrmidon for getting me to update this!

v1.9.1
 - Alt-Clicking the Macro will now summon a ground mount.
 - Seahorse will now only show up if the macro is ran while you are swimming.
 - Moved some previously flying-only mounts into shifting, as they now work for both ground and flying.
 - Fix for ground mounts inside of Nespirah.
 - Fixed some swimming bugs.

v1.9
 - MiniMount now supports Holiday Mounts! During a Holiday, MiniMount will automatically select the Mount for that Holiday Season, if you have it. IE: It will only load a Brewfest Mount during Brewfest or the Headless Horseman's Mount during Hallow's Eve.
 - Merged all Ground and Flying mounts, since they now all shift based on riding skill.
 - Removed annoying debug "summoning" message.
 - Depreciated the old command-lines.
 - Fixed a bug with Wintergrasp that wouldn't bring up a flying mount if there wasn't a battle going on.
 - All flying carpets are now shifting mounts.
 - Added support for the Seahorse in Vashj'ir!

v1.8.1
- Offical 4.0.x release.
- Finally Fixed the AQ Mount system!
- The MiniMount Macro will no longer cancel shapeshift forms when middle-clicking for a new mount.
- Merged & removed FastEpicFlying. (The Master Riding skill replaces the Artisan riding skill.)
- Added "help" to the command list.
- Rebuilt detection systems.

v1.8
- Updated for 3.3.5!
- Reworked the flying detection method. You can now fly from the top of the Violet Citadel, and use a ground mount at the bottom!
- Added several new and forgetten mounts, including the Celestrial Steed!
- Mounts that change flying speed will now show up with your other fast epic flying mounts!
- The MiniMount Macro now supports the Dragon Essences in The Oculus!
- Fixed a bug with the Blue Drake and the MiniMount Macro.
- Added German Localization! Thanks eNBeWe!

v1.7.1
- Minor Bugfixes

v1.7.0
- Added several new/forgotten mounts to the Database, including the new Onyxian Mount!
- 310% speed Mounts are now in a new Fast Epic Flying category!
- Fixed the AQ bug where it shows up in the rotation, however, I had to disable the AQ mounts for the time being.
- MiniMount will now only complain about a lack of mounts when it's called, not when the macro wants to change.

v1.6.8
- MiniMount will now work properly if you are in Wintergrasp during it's combat phase.

v1.6.7b
- Fixed a bug that wouldn't let toons fly in Northrend if they had Northrend Flying, but were not yet level 77.

v1.6.7a
- MiniMount's Command-line options will now accept Macro Conditionals!
- Fixed a bug in the new macro that wouldn't display the proper mount when updated.

v1.6.7
- Updated for Patch 3.2
- Added the new 3.2 mounts to the Database.
- Removed Wintergrasp's Flying restriction.
- Added the "New" commandline to cycle through next mounts.
- You can now Middle-Click the macro to choose a different next mount!
- Fixed a bug that wouldn't generate an options database.
- Fixed a bug where if you don't have mounts to match your riding skill, MiniMount will choose the next available mount.

v1.6.6
- Fixed a bug that would occasionally give people an error upon logging in.

v1.6.5
- Updated TOC for 3.1
- Added a multitude of new mounts to the database.
- Removed the swimming error.
- Added a localized Name Adjustment Database for mis-named mounts. This will correct the mount names so the macro correctly shows which mount is next.
- Added the Bronze Drake to the English Name Adjustment Database.
- Fixed a bug that would refresh the macro far more often than it should of done.

v1.6
- Added several new mounts to the Database.
- The Obsidian Sanctum and the Eye of Eternity are now on the No-Fly Zone list.
- You can now mount your flying mount at the top of the Violet Citadel.
- Druids & Shamans Rejoice! The MiniMount Macro will now cancel shapeshift forms!
- Changed the "Detect New Mount" function. It should now only run when you learn a new mount.
- MiniMount will no longer attempt to run if you are not yet level 30.
- Fixed a bug that wouldn't make it so the options wouldn't update.
- Moved the Options to a Per-Character Database, as all options would be specific to the characters.
- Fixed a bug that would sometimes report an error when first logging into WoW.
- Fixed a bug that would spew an error if you only had shifting mounts.
- Known Bug: AQ Mounts will occaisionally get thrown into the mount rotation.

v1.5b
- Added The Occulus to the No-Fly Zone list.
- Added White Polar Bear Mount to the Database.
- Fixed a bug that would break MiniMount if it detected a Death Knight's flyer, until you reloaded the UI.
- Fixed a bug that generated an error when entering Krasus' Landing if you could fly, but didn't have Northrend Flying.

v1.5a
- Added Wintergrasp as a No-Fly zone.
- Added the Armored Snowy Gryphon to the database.
- Fixed a bug that would screw things up if you could fly in Northrend.
- Fixed the mount function so that unknown mounts WILL be added to the mount list, as was originally intended.

v1.5
- MiniMount now knows if you've gotten your Northrend Flying training or not!
- Added a "Ground" function to the MiniMount_Mount function. This will select the appropriate ground mount for you.
- Moved the Winged Steed of the Ebon Blade to the proper Shifting group, since this mount scales with a Death Knight's Riding Skill.
- The Swimming Error is now properly named in the localization file.
- You can now right-click the macro to dismount. You must hold down the ctrl button to dismount in the air. Holding down ctrl when pressing the button will always dismount you.
- Minimount will now update when changing continents.
- The Macro will now auto-update with the proper mounts when logging on a toon.
- Added "Fly Zones", allowing MiniMount to call up a new mount if you've entered an area that may or may not support flying. (Most of Dalaran, for example.) This needs to be localized for non-English clients!
- Cleaned up some internal code to make things easier on everyone.

v1.4b
- Major bugfixes involving multiple toons & the new macro.

v1.4a
- Fixed a bug that would occur if you had no mounts.

v1.4
- MiniMount will no longer attempt to run if you are swimming.
- Added Riding Skill name localization for every locale. This should enable MiniMount to work with any client. (If it does not work with your client, send me the proper name for the Riding skill.)
- Added the DeathKnight Mount to the database.
- MiniMount will now create a mounting macro for you! This macro (called "MiniMount") will automagically update to show you which mount you will have next!

v1.3
- Added the Ahn'Qiraj mounts to a special database. These mounts will only show up in the rotation while in AQ40.
- Added a feature that will record which mounts are reported as Unknown Mounts, so you won't get spammed by the message everytime you mount.
- Fixed a bug that would occur if you specifically called for a mount-type you didn't have any mounts of.
- MiniMount will now auto-update itself when you learn new mounts. (Note: This may also occur when learning non-combat pets.)

v1.2a
- Bugfix for previous release.

v1.2
- Added Paladin & Warlock Mounts to the Database.
- Fixed a bug in the Scaling Mount code that wouldn't add it to the Mount Arrays.

v1.1
- Fixed a bug that would result in an error, not letting you mount.
- Added extra redundancies, to either mount you, or give you an error on similar problems.

v1.0
- Initial Release
- Partial WotLK support 