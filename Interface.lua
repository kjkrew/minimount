--[[
	MiniMount Interface Functions
	Version <%version%>
	
	Revision: $Id: Interface.lua 5 2016-09-26 01:35:24 Pacific Daylight Time Kjasi $
]]

local m = _G.MiniMount
if (not m) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Global.|r")
	return
end
local L = m.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Unable to find MiniMount Localization.|r")
	return
end
local LibKJ = LibStub("LibKjasi-1.0")

local BUTTONTYPE_LARGELIST = 1
local BUTTONTYPE_LIST = 2
local BUTTONTYPE_ICONS = 3

local BUTTONCOUNT_LARGELIST = 15
local BUTTONCOUNT_LIST = 28
local BUTTONCOUNT_ICONS = 50

local NumCompansionsPerPage = BUTTONCOUNT_LARGELIST
local MaxPerPage = 50	-- Maximum number in our XML file

function m:Tooltip_Generate(frame,title,text,anchor)
	if anchor == nil then
		anchor = "ANCHOR_CURSOR"
	end
	if title == nil then
		title = "nil"
	end
	if text == nil then
		text = "nil"
	end

	MiniMountTooltip:SetOwner(frame, anchor)
	MiniMountTooltip:SetText(title)
	MiniMountTooltip:AddLine(text,1,1,1,true)
	MiniMountTooltip:Show()
end

function m:InterfaceLoaded()
	if (not m.Ready) then return end
	if (not MiniMount_Options["ML_ListType"]) then
		m.Interface.ButtonType = BUTTONTYPE_LARGELIST
		MiniMount_Options["ML_ListType"] = BUTTONTYPE_LARGELIST
	else
		m.Interface.ButtonType = MiniMount_Options["ML_ListType"]
	end
	
	for i=1,BUTTONTYPE_ICONS do
		local button = _G["MiniMount_MLCompanionListTypeButton"..i]
		button.ListType = i
		if (i == BUTTONTYPE_LIST) then
			button.IconTexture:SetTexCoord(0,0.234375,0,0.9375)
		elseif (i == BUTTONTYPE_ICONS) then
			button.IconTexture:SetTexCoord(0.46875,0.703125,0,0.9375)
		else
			-- Defaults to BUTTONTYPE_LARGELIST 
			button.IconTexture:SetTexCoord(0.234375,0.46875,0,0.9375)
		end
	end
	
	hooksecurefunc("CollectionsJournal_UpdateSelectedTab", MiniMount_MountList_UpdateSelectedTab)
	
end

--== Updated functions ==--
function MiniMount_MountList_UpdateSelectedTab(self)	
	local selected = PanelTemplates_GetSelectedTab(self);

	MountJournal:SetShown(selected == 1);
	PetJournal:SetShown(selected == 2);
	ToyBox:SetShown(selected == 3);
	HeirloomsJournal:SetShown(selected == 4);
	MiniMount_MountList:SetShown(selected == 5);

	if ( selected == 1 ) then
		CollectionsJournalTitleText:SetText(MOUNTS);
	elseif (selected == 2 ) then
		CollectionsJournalTitleText:SetText(PET_JOURNAL);
	elseif (selected == 3 ) then
		CollectionsJournalTitleText:SetText(TOY_BOX);
	elseif (selected == 4 ) then
		CollectionsJournalTitleText:SetText(HEIRLOOMS);
	elseif (selected == 5 ) then
		CollectionsJournalTitleText:SetText(m.MINIMOUNT_TITLE);
	end

	--self.HeirloomTabHelpBox:SetShown(ShouldShowHeirloomTabHelpTip());
end

--== Mount List Functions ==--

function m:MountList_OnLoad(self)
	self:RegisterEvent("VARIABLES_LOADED")
	self:RegisterEvent("COMPANION_LEARNED")
	self:RegisterEvent("COMPANION_UNLEARNED")
	--self:RegisterEvent("COMPANION_UPDATE")
end

local function LoadCollections()


end


function m:MountList_OnEvent(self, event, ...)
	if ( event == "COMPANION_LEARNED" or event == "COMPANION_UNLEARNED" ) then -- or event == "COMPANION_UPDATE"
		if (MiniMount_MountList:IsVisible()) then
			m:MountList_UpdateList()
		end
	elseif (event == "VARIABLES_LOADED") then
		if not CollectionsJournal then
			--local BCol = UIParentLoadAddOn("Blizzard_Collections")
			return
		end
	
		local button = CreateFrame("Button", "MiniMountJournal", CollectionsJournal, "CollectionsJournalTab")
		button:SetText(m.MINIMOUNT_TITLE)
		button:Show()

		--LibKJ:printr(PetJournalParent:GetName())
	
		--LibKJ:AddWindowTab(button,MiniMount_MountList,PetJournalParent)
		self:UnregisterEvent("VARIABLES_LOADED")
	end
end

function m:MountList_OnShow(self)
	m:MountList_UpdateList()
end

function m:MountList_UpdateList()
	local button, iconTexture, id
	local creatureID, creatureName, spellID, icon, active
	local offset, selected
	
	if (not selected) then
		selected = 1
		--creatureID = GetCompanionInfo(MiniMount_MountList.mode, selected)
		--MiniMount_MountList.idMount = creatureID
	end

	-- Show/Hide unused buttons
	for i=1,MaxPerPage do
		button = _G["MiniMount_MLCompanionButton"..i]
		if button then
			if i>NumCompansionsPerPage then
				button:Hide()
			else
				button:Show()
			end
		end
	end
end













--[[ Older Functions ]]--








function MiniMount_ChangeSpellBookInterface(type)
	if (m.Interface.SpellBookOverride) then
		if type then
			if strlower(type) == strlower(tostring(BOOKTYPE_MOUNT)) then
				SpellBookCompanionsFrame:Hide()
				MiniMount_MountList:Show()
			else
				MiniMount_MountList:Hide()
			end
		else
			MiniMount_MountList:Hide()
		end
	end
	
	--[[
	if (SpellBookCompanionsFrame:IsVisible()) then
		MiniMount_MountList:Hide()
	end
	]]
end

--== XML Functions ==--
--Interface Options
function m:InterfaceOptions_onLoad()
	-- Register the Interface Options page
	MiniMount_InterfaceOptionsFrame.name = "MiniMount"
	InterfaceOptions_AddCategory(MiniMount_InterfaceOptionsFrame)

	-- Localizations
	MiniMount_InterfaceOptionsFrame_Title:SetText(m.MINIMOUNT_TITLE)
	MiniMount_InterfaceOptionsFrame_UseHolidayText:SetText(L["Use Holiday Mounts Title"])
	MiniMount_InterfaceOptionsFrame_DismountWhileFlyingText:SetText(L["Dismount While Flying Title"])
	MiniMount_InterfaceOptionsFrame_IncludeFlyingWithGroundText:SetText(L["Include Flying with Ground Title"])
end

function m:InterfaceOptions_onShow()
	-- Set Text
	MiniMount_InterfaceOptionsFrame_Version:SetText(format(L["Version"], m.Version))

	-- Set Checkboxes
	MiniMount_InterfaceOptionsFrame_UseHolidayCheckBox:SetChecked(MiniMount_Options.Global.UseHolidayMounts)
	MiniMount_InterfaceOptionsFrame_DismountWhileFlyingCheckBox:SetChecked(MiniMount_Options.Global.DismountWhileFlying)
	MiniMount_InterfaceOptionsFrame_IncludeFlyingWithGroundCheckBox:SetChecked(MiniMount_Options.Global.UseFlyingWithGround)


end

function MiniMount_Options_CheckboxOnOff(checkbox, value)
	if (value == nil) then
		value = false
	end

	if (checkbox == "MiniMount_InterfaceOptionsFrame_UseHoliday") then
		MiniMount_Options.Global.UseHolidayMounts = value
		m:SelectNextMount(m.CurrentTypes, true)
		MiniMount_Macro()
	elseif (checkbox == "MiniMount_InterfaceOptionsFrame_DismountWhileFlying") then
		MiniMount_Options.Global.DismountWhileFlying = value
		MiniMount_Macro()
	elseif (checkbox == "MiniMount_InterfaceOptionsFrame_IncludeFlyingWithGround") then
		MiniMount_Options.Global.UseFlyingWithGround = value
		m:SelectNextMount(m.CurrentTypes, true)
		MiniMount_Macro()
	end
end







--== [[ SpellBook Functions we copied for our Spellbook ]] ==--


function MiniMount_SpellBook_Update()
	if (not SpellBookFrame.bookType) then
		return
	end	
	if (m.Interface.ButtonType == BUTTONTYPE_LIST) then
		NumCompansionsPerPage = BUTTONCOUNT_LIST
	elseif (m.Interface.ButtonType == BUTTONTYPE_ICONS) then
		NumCompansionsPerPage = BUTTONCOUNT_ICONS
	else
		-- Defaults to LargeList
		NumCompansionsPerPage = BUTTONCOUNT_LARGELIST
	end
	
	MiniMount_ChangeSpellBookInterface(SpellBookFrame.bookType)
	
	MiniMount_MLUpdateCompanionsFrame(BOOKTYPE_MOUNT)
	MiniMount_MountList_UpdateCompanionPreview()
end

function MiniMount_MountList_OnLoad(self)
	self:RegisterEvent("COMPANION_LEARNED")
	self:RegisterEvent("COMPANION_UNLEARNED")
	self:RegisterEvent("COMPANION_UPDATE")
	self:RegisterEvent("SPELL_UPDATE_COOLDOWN")
	self:RegisterEvent("UNIT_ENTERED_VEHICLE")
end

function MiniMount_MountList_OnEvent(self, event, ...)
	local arg1 = ...
	if ( event == "SPELL_UPDATE_COOLDOWN" ) then
		if ( self:IsVisible() ) then
			MiniMount_MLUpdateCompanionCooldowns()
		end	
	elseif ( event == "COMPANION_LEARNED" ) then
		if (MiniMount_MountList:IsVisible() ) then
			MiniMount_SpellBook_Update()
		end
	elseif ( event == "COMPANION_UNLEARNED" ) then
		local page
		local numCompanions = GetNumCompanions(MiniMount_MountList.mode)
		page = SPELLBOOK_PAGENUMBERS[BOOKTYPE_MOUNT]
		if ( numCompanions > 0 ) then
			MiniMount_MountList.idMount = GetCompanionInfo("MOUNT", 1)
			MiniMount_MountList_UpdateCompanionPreview()
		else
			MiniMount_MountList.idMount = nil
		end
		if (MiniMount_MountList:IsVisible()) then
			MiniMount_SpellBook_Update()
		end
	elseif ( event == "COMPANION_UPDATE" ) then
		if ( not MiniMount_MountList.idMount ) then
			MiniMount_MountList.idMount = GetCompanionInfo("MOUNT", 1)
		end
		if (self:IsVisible()) then
			MiniMount_MLUpdateCompanionsFrame()
		end
	elseif ( (event == "UNIT_ENTERED_VEHICLE" or event == "UNIT_EXITED_VEHICLE") and (arg1 == "player")) then
		MiniMount_MLUpdateCompanionsFrame()
	end
end

function MiniMount_MountList_FindCompanionIndex(creatureID, mode)
	if (not MiniMount_MountList:IsVisible()) then return end
	if ( not mode ) then
		mode = MiniMount_MountList.mode
	end
	if (not creatureID ) then
		creatureID = MiniMount_MountList.idMount
	end
	for i=1,GetNumCompanions(mode) do
		if ( GetCompanionInfo(mode, i) == creatureID ) then
			return i
		end
	end
	return nil
end

function MiniMount_MountList_UpdateCompanionPreview()
	local selected = MiniMount_MountList_FindCompanionIndex()
	
	if (selected) then
		local creatureID, creatureName, spellID = GetCompanionInfo(MiniMount_MountList.mode, selected)
		if (MiniMount_MLCompanionModelFrame.creatureID ~= creatureID) then
			MiniMount_MLCompanionModelFrame.creatureID = creatureID
			MiniMount_MLCompanionModelFrame:SetCreature(creatureID)
			MiniMount_MLCompanionSelectedName:SetText(creatureName)
			
			-- Update side-bar data
			local mount = m:GetDBMountName(spellID)
			if mount then
				local mdata = MiniMount_Mounts_db[mount]
				local type_s = ""
				local speed_s = ""
				
				local typearray = {}
				local swimarray = {}
				local speedarray = {}

				for k,type in pairs(mdata["types"]) do
					if (type == m.TYPE_SWIM60) or (type == m.TYPE_SWIM100) or (type == m.TYPE_SWIM300) or (type == m.TYPE_SWIM450) then
						if (not typearray[type]) then typearray[type] = L["Type Swimming"] end
						if (type == m.TYPE_SWIM60) then
							swimarray[type] = m.Speed.s60
						elseif (type == m.TYPE_SWIM100) then
							swimarray[type] = m.Speed.s100
						elseif (type == m.TYPE_SWIM300) then
							swimarray[type] = m.Speed.s300
						else
							swimarray[type] = m.Speed.s450
						end
					elseif (type == m.TYPE_SHIFTING) then
						if (not typearray[type]) then typearray[type] = L["Type Shifting"] end
						speedarray[type] = L["Speed Adaptive"]
					elseif (type == m.TYPE_GROUNDSLOW) then
						if (not typearray[type]) then typearray[type] = L["Type Ground Slow"] end
						speedarray[type] = m.Speed.s0
					elseif (type == m.TYPE_FLYING) then
						if (not typearray[type]) then typearray[type] = L["Type Flying"] end
						if (m.RidingSkill == m.RIDINGSKILLLVL_EXPERT) then
							speedarray[type] = m.Speed.s150
						elseif (m.RidingSkill == m.RIDINGSKILLLVL_ARTISAN) then
							speedarray[type] = m.Speed.s280
						else
							speedarray[type] = m.Speed.s310
						end
					else
						if (not typearray[type]) then typearray[type] = L["Type Ground"] end
						if (m.RidingSkill == m.RIDINGSKILLLVL_APPRENTICE) then
							speedarray[type] = m.Speed.s60
						else
							speedarray[type] = m.Speed.s100
						end
					end
				end
				
				local count = 0
				for type,value in pairs(typearray) do
					if count == 0 then
						type_s = value
					else
						type_s = format(L["Display List Array"],type_s,value)
					end
				end
				count = 0
				if (LibKJ:tcount(speedarray)>0) then
					local min = 0
					local max = 0
					for type,speed in pairs(speedarray) do
						if (min==0) then
							min = speed
							max = speed
						end
						if (speed<min) then
							min = speed
						end
						if (speed>min) then
							max = speed
						end
					end
					if (min == max) then
						if count == 0 then
							speed_s = min
						else
							speed_s = format(L["Display List Array"],speed_s,min)
						end
						count = count + 1
					else
						if count == 0 then
							speed_s = min
						else
							speed_s = format(L["Display Speed Dash"],min,max)
						end
						count = count + 1
					end
				end
				
				if (LibKJ:tcount(swimarray)>0) then
					local min = 0
					local max = 0
					for type,speed in pairs(swimarray) do
						if (min==0) then
							min = speed
							max = speed
						end
						if (speed<min) then
							min = speed
						end
						if (speed>min) then
							max = speed
						end
					end
					if (min == max) then
						if count == 0 then
							speed_s = min
						else
							speed_s = format(L["Display List Array"],speed_s,min)
						end
						count = count + 1
					else
						if count == 0 then
							speed_s = min
						else
							speed_s = format(L["Display Speed Dash"],min,max)
						end
						count = count + 1
					end
				end
				
				local seats = 1
				if (mdata["seats"]) then seats = mdata["seats"] end
				local area = L["Usable Anywhere"]
				if (mdata["zones"]) then
					for k,z in pairs(mdata["zones"]) do
						if k==1 then
							area = GetMapNameByID(z)
						else
							area = area..", "..GetMapNameByID(z)
						end
					end
				end
				
				
				
				local text = format(L["Mount List Display Data"],type_s,speed_s,seats,area)
				if (mdata["holiday"]) then
					text = text..format(L["Mount List Display Data Holiday"],mdata["holiday"])
				end
				MiniMount_MLCompanionDataText:SetText(text)
			else
				MiniMount_MLCompanionDataText:SetText("Nothing to see here")
			end
		end
	end
end

function MiniMount_MLUpdateCompanionsFrame(type)
	if (not MiniMount_MountList:IsVisible()) then return end
	local button, iconTexture, id
	local creatureID, creatureName, spellID, icon, active
	local offset, selected
	
	if (type) then
		MiniMount_MountList.mode = type
	end
	
	if (not MiniMount_MountList.mode) then
		return
	end
	
	MiniMount_ML_UpdatePages()
	
	local currentPage, maxPages = MiniMount_ML_GetCurrentPage()
	if (currentPage) then
		currentPage = currentPage - 1
	end
	
	offset = (currentPage or 0)*NumCompansionsPerPage
	selected = MiniMount_MountList_FindCompanionIndex(MiniMount_MountList.idMount)
	
	if (not selected) then
		selected = 1
		creatureID = GetCompanionInfo(MiniMount_MountList.mode, selected)
		MiniMount_MountList.idMount = creatureID
	end

	-- Show/Hide unused buttons
	for i=1,MaxPerPage do
		button = _G["MiniMount_MLCompanionButton"..i]
		if button then
			if i>NumCompansionsPerPage then
				button:Hide()
			else
				button:Show()
			end
		end
	end
	
	for i = 1, NumCompansionsPerPage do
		button = _G["MiniMount_MLCompanionButton"..i]
		-- Place/Move & Scale Buttons
		if (m.Interface.ButtonType == BUTTONTYPE_LIST) then
			button:SetScale(0.75)
			button.TextBackground:Show()
			button.ButtonIndex = i
			if (i==1) then
				button:SetPoint("TOPLEFT","MiniMount_MountList","TOPLEFT",101,-312)
			else
				local div = (floor(i/4)*4) + 1
				if i==div then
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-4),"BOTTOMLEFT",0,-8)
				else
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-1),"TOPLEFT",150,0)
				end
			end
		elseif (m.Interface.ButtonType == BUTTONTYPE_ICONS) then
			button:SetScale(1)
			button.TextBackground:Hide()
			if (i==1) then
				button:SetPoint("TOPLEFT","MiniMount_MountList","TOPLEFT",76,-240)
			else
				local div = (floor(i/10)*10) + 1
				if i==div then
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-10),"BOTTOMLEFT",0,-8)
				else
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-1),"TOPRIGHT",8,0)
				end
			end
		else
			-- Defaults to LargeList
			button:SetScale(1)
			button.TextBackground:Show()
			if (i==1) then
				button:SetPoint("TOPLEFT","MiniMount_MountList","TOPLEFT",76,-240)
			else
				local div = (floor(i/3)*3) + 1
				if i==div then
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-3),"BOTTOMLEFT",0,-8)
				else
					button:SetPoint("TOPLEFT", "MiniMount_MLCompanionButton"..(i-1),"TOPLEFT",150,0)
				end
			end
		end
		id = i + (offset or 0)
		creatureID, creatureName, spellID, icon, active = GetCompanionInfo(MiniMount_MountList.mode, id)
		button.creatureID = creatureID
		button.spellID = spellID
		button.active = active
		if ( creatureID ) then
			button.IconTexture:SetTexture(icon)
			button.IconTexture:Show()
			if (m.Interface.ButtonType == BUTTONTYPE_ICONS) then
				button.SpellName:SetText(nil)
				button.SpellName:Hide()
			else
				button.SpellName:SetText(creatureName)
				button.SpellName:Show()
			end
			button:Enable()
		else
			button:Disable()
			button.IconTexture:Hide()
			button.SpellName:Hide()
		end
		if ( (id == selected) and creatureID ) then
			button:SetChecked(true)
		else
			button:SetChecked(false)
		end
		
		if ( active ) then
			button.ActiveTexture:Show()
		else
			button.ActiveTexture:Hide()
		end
		button.Background:SetTexCoord(0.62304688, 0.70703125, 0.00390625, 0.17187500)
	end
	
	if ( selected ) then
		creatureID, creatureName, spellID, icon, active = GetCompanionInfo(MiniMount_MountList.mode, selected)
		if ( active and creatureID ) then
			MiniMount_MLCompanionSummonButton:SetText(MiniMount_MountList.mode == "MOUNT" and BINDING_NAME_DISMOUNT or PET_DISMISS)
		else
			MiniMount_MLCompanionSummonButton:SetText(MiniMount_MountList.mode == "MOUNT" and MOUNT or SUMMON)
		end
	end
	
	MiniMount_MLUpdateCompanionCooldowns()
end

function MiniMount_MLUpdateCompanionCooldowns()
	local currentPage, maxPages = MiniMount_ML_GetCurrentPage()
	if (currentPage) then
		currentPage = currentPage - 1
	end
	local offset = (currentPage or 0)*NumCompansionsPerPage
	
	for i = 1, NumCompansionsPerPage do
		local button = _G["MiniMount_MLCompanionButton"..i]
		local cooldown = _G[button:GetName().."Cooldown"]
		if ( button.creatureID ) then
			local start, duration, enable = GetCompanionCooldown(MiniMount_MountList.mode, offset + button:GetID())
			if ( start and duration and enable ) then
				CooldownFrame_SetTimer(cooldown, start, duration, enable)
			end
		else
			cooldown:Hide()
		end
	end
end

function MiniMount_MLCompanionButton_OnLoad(self)
	self:RegisterForDrag("LeftButton")
	self:RegisterForClicks("LeftButtonUp", "RightButtonUp")
end

function MiniMount_MLCompanionButton_OnEnter(self)
	if ( GetCVar("UberTooltips") == "1" ) then
		GameTooltip_SetDefaultAnchor(GameTooltip, self)
	else
		GameTooltip:SetOwner(self, "ANCHOR_LEFT")
	end

	if ( GameTooltip:SetSpellByID(self.spellID) ) then
		self.UpdateTooltip = CompanionButton_OnEnter
	else
		self.UpdateTooltip = nil
	end
	
	GameTooltip:Show()
end

function MiniMount_MLCompanionButton_OnClick(self, button)
	local selectedID = MiniMount_MountList.idMount

	if ( button ~= "LeftButton" or ( selectedID == self.creatureID) ) then
		local currentPage, maxPages = MiniMount_ML_GetCurrentPage()
		if (currentPage) then
			currentPage = currentPage - 1
		end
		
		local offset = (currentPage or 0)*NumCompansionsPerPage
		local index = self:GetID() + offset
		if ( self.active ) then
			DismissCompanion(MiniMount_MountList.mode)
		else
			CallCompanion(MiniMount_MountList.mode, index)
		end
	else
		MiniMount_MountList.idMount = self.creatureID
		MiniMount_MountList_UpdateCompanionPreview()
	end
	
	MiniMount_MLUpdateCompanionsFrame()
end

function MiniMount_MLCompanionButton_OnModifiedClick(self)
	local id = self.spellID
	if ( IsModifiedClick("CHATLINK") ) then
		if ( MacroFrame and MacroFrame:IsShown() ) then
			local spellName = GetSpellInfo(id)
			ChatEdit_InsertLink(spellName)
		else
			local spellLink = GetSpellLink(id)
			ChatEdit_InsertLink(spellLink)
		end
	elseif ( IsModifiedClick("PICKUPACTION") ) then
		MiniMount_MLCompanionButton_OnDrag(self)
	end
end

function MiniMount_MLCompanionButton_OnDrag(self)
	local currentPage, maxPages = MiniMount_ML_GetCurrentPage()
	if (currentPage) then
		currentPage = currentPage - 1
	end
	
	local offset = (currentPage or 0)*NumCompansionsPerPage
	local dragged = self:GetID() + offset
	PickupCompanion( MiniMount_MountList.mode, dragged )
end

function MiniMount_MLCompanionSummonButton_OnClick()
	local selected = MiniMount_MountList_FindCompanionIndex()
	local creatureID, creatureName, spellID, icon, active = GetCompanionInfo(MiniMount_MountList.mode, selected)
	if ( active ) then
		DismissCompanion(MiniMount_MountList.mode)
		PlaySound("igMainMenuOptionCheckBoxOn")
	else
		CallCompanion(MiniMount_MountList.mode, selected)
		PlaySound("igMainMenuOptionCheckBoxOff")
	end
end

function MiniMount_ML_UpdatePages()
	local currentPage, maxPages = MiniMount_ML_GetCurrentPage()
	if ( maxPages == nil or maxPages == 0 ) then
		return
	end
	if ( currentPage > maxPages ) then
		SPELLBOOK_PAGENUMBERS[SpellBookFrame.bookType] = maxPages
		currentPage = maxPages
		if ( currentPage == 1 ) then
			SpellBookPrevPageButton:Disable()
		else
			SpellBookPrevPageButton:Enable()
		end
		if ( currentPage == maxPages ) then
			SpellBookNextPageButton:Disable()
		else
			SpellBookNextPageButton:Enable()
		end
	end
	if ( currentPage == 1 ) then
		SpellBookPrevPageButton:Disable()
	else
		SpellBookPrevPageButton:Enable()
	end
	if ( currentPage == maxPages ) then
		SpellBookNextPageButton:Disable()
	else
		SpellBookNextPageButton:Enable()
	end
	SpellBookPageText:SetFormattedText(PAGE_NUMBER, currentPage)
end

function MiniMount_ML_GetCurrentPage()
	local currentPage, maxPages
	local numPetSpells = HasPetSpells() or 0
	
	currentPage = SPELLBOOK_PAGENUMBERS[SpellBookFrame.bookType]
	maxPages = ceil(GetNumCompanions(MiniMount_MountList.mode)/NumCompansionsPerPage)
	return currentPage, maxPages
end

function MiniMount_MLListTypeButton_OnClick(self, button)
	local buttonType = m.Interface.ButtonType

	if (buttonType ~= self.ListType) then
		MiniMount_Options["SpellBookListType"] = self.ListType
		m.Interface.ButtonType = self.ListType
		MiniMount_SpellBook_Update()
	end
end